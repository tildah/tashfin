const coBody = require("co-body");
const parser = require('url');
const C = require("colors");

const getQuery = url => parser.parse(url, true).query;

const getPath = url => decodeURIComponent(parser.parse(url, true).pathname);

const logRequest = (url, method, moduleName, action, logRequestDate, params) => {
  var str = '';
  str += C.green(method);
  str += ' ' + C.yellow(url);
  str += (logRequestDate ? ' [' + new Date() + ']' : ' | ');
  str += ' Module Name: ' + C.cyan(moduleName);
  str += ' | Action: ' + C.magenta(action);
  str += ' | Params: ' + C.italic(params);
  return str;
}

const parseBody = async (req, options = {}) => {
  try {
    const body = await coBody(req, options);
    const isJSON = req.headers["Content-Type"] === "application/json";
    if (isJSON) return JSON.parse(body);
    return body;
  } catch (error) {
    console.log(error)
    return {};
  }
}

module.exports = {
  getQuery,
  getPath,
  logRequest,
  parseBody,
}
