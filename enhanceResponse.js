const mime = require('mime-types');
const fs = require('fs');

module.exports = res => {

  res.json = object => {
    res.setHeader("Content-Type", "application/json");
    res.end(JSON.stringify(object))
  }

  res.redir = location => {
    res.statusCode = 302;
    res.setHeader("Location", location);
  }

  res.reload = (req, fallback) => {
    res.redir(req.headers.referer || fallback)
  }

  res.asFile = (content, ext) => {
    res.setHeader("Content-Type", mime.contentType(ext));
    res.end(content)
  }

  res.file = path => {
    if (!fs.existsSync(filePath)) throw { statusCode: 404, msg: "Cannot find file" };
    const fileContent = fs.readFileSync(filePath);
    const fileBase = path.basename(filePath);
    res.asFile(fileContent, fileBase);
  }

  res.download = (filePath, fileName) => {
    var att = 'attachment';
    if (fileName) att += '; filename="' + fileName + '"';
    res.setHeader("Content-Disposition", att);
    res.file(filePath);
  }

}
