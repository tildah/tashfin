const http = require("http");
const helpers = require('./helpers');
const enhanceResponse = require("./enhanceResponse");

const parseUrl = (path, noHomeRoot) => {
  const [, ...parts] = path.split("/");
  const [moduleName, ...sndLevelParams] = parts;
  const [action, ...trdLevelParams] = sndLevelParams;

  const eventuals = [];
  const homeRoot = { moduleName: "", action: "root", params: parts }

  if(moduleName) eventuals.push(...[
    { moduleName: "", action: moduleName, params: sndLevelParams },
    { moduleName, action: "root", params: sndLevelParams }
  ])
  if(moduleName && !sndLevelParams.length) eventuals.push({
    moduleName, action: "$root", params: [] 
  })

  if(action) eventuals.push({ moduleName, action, params: trdLevelParams })
  return [
    ...eventuals.reverse(),
    ...(!noHomeRoot || !eventuals.length ? [homeRoot] : []),
  ]
}

const getOperationName = (method, action) => `${method}_${action}`;

const defaultOnError = (e, req, res) => {
  console.error(e);
  res.setHeader("Content-Type", "text/plain")
  res.end(e.toString());
}

const runOperation = async (req, res, options) => {
  const {
    modules,
    logRequest,
    logRequestDate,
    noHomeRoot,
    onError = defaultOnError
  } = options;
  try {
    const {url, method} = req;
    const path = helpers.getPath(decodeURIComponent(url));
    const eventuals = parseUrl(path, noHomeRoot);
    const selectedParsing = eventuals.find(e => {
      const opName = getOperationName(method, e.action);
      return modules[e.moduleName] && modules[e.moduleName][opName];
    })
    const {moduleName, action, params} = selectedParsing || {};
    if(logRequest) console.log(helpers.logRequest(
      req.url, req.method, moduleName, action, logRequestDate, params
    ))
    if(!selectedParsing) throw {statusCode: 404};
    const opName = getOperationName(req.method, action);
    const operation = modules[moduleName][opName];
    await operation(req, res, ...params);
  } catch(e) {
    res.statusCode = e.statusCode ?? 500;
    onError(e, req, res);
  }
}

const predefinedHeaders = () => ({
  "Content-Type": "text/html"
})

const callback = options => async (req, res) => {
  const { defaultHeaders = () => ({}) } = options; 
  const headers = { ...predefinedHeaders(), ...defaultHeaders(req) } 
  Object.keys(headers).forEach(header => {
    res.setHeader(header, headers[header]);
  })

  res.statusCode = 200;
  enhanceResponse(res);
  await runOperation(req, res, options);
}

const createServer = options => http.createServer(callback(options));

const listen = options => {
  const {port = 3000} = options
  const server = http.createServer(callback(options));
  server.listen(port, () => {
    console.log("Server listening on: http://localhost:%s", port);
  });
}

module.exports = {
  listen,
  createServer,
  ...helpers,
}

